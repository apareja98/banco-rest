package co.edu.usbcali.banco.rest.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.edu.usbcali.banco.domain.Cliente;
import co.edu.usbcali.banco.domain.Cuenta;
import co.edu.usbcali.banco.rest.dto.ClienteDTO;
import co.edu.usbcali.banco.rest.dto.ClienteLoginDTO;
import co.edu.usbcali.banco.rest.dto.ClienteLoginRespuestaDTO;
import co.edu.usbcali.banco.rest.dto.RespuestaDTO;
import co.edu.usbcali.banco.rest.mapper.ClienteMapper;
import co.edu.usbcali.banco.services.ClienteService;

@RestController
@CrossOrigin
@RequestMapping("/cliente")
public class ClienteRestController {
	
	@Autowired
	private ClienteService clienteService;
	@Autowired
	private ClienteMapper ClienteMapper;
	
	@GetMapping("/findAll")
	public List<ClienteDTO> findAll(){
		List<ClienteDTO> listaClienteDTO = new ArrayList<>();
		try {
			List<Cliente> listaCliente= clienteService.findAll();
			listaClienteDTO =  ClienteMapper.listaClienteToClienteDTO(listaCliente);
		} catch (Exception e) {
		
		}
		return listaClienteDTO; 
	}
	
	@GetMapping("/findById/{id}")
	public ClienteDTO findById(@PathVariable("id")Long id) {
		try {
			Cliente cliente = clienteService.findById(id);
			if(cliente==null) {
				return null;
			}
			return ClienteMapper.clienteToClienteDTO(cliente);
		} catch (Exception e) {
			// TODO: handle exception
		}
		return null;
	}
	@PostMapping("/save")
	public ResponseEntity<RespuestaDTO> save(@RequestBody ClienteDTO clienteDTO){
		RespuestaDTO respuesta =new RespuestaDTO();
		try {
			Cliente cliente = ClienteMapper.clienteDTOtoCliente(clienteDTO);
			clienteService.save(cliente);
			respuesta.setCodigo("200");
			respuesta.setMensaje("El cliente se creó con exito");
			return new ResponseEntity<RespuestaDTO>(respuesta,HttpStatus.OK);
		} catch (Exception e) {
			respuesta.setCodigo("500");
			respuesta.setMensaje(e.getMessage());
			return new ResponseEntity<RespuestaDTO>(respuesta, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	@DeleteMapping("/delete/{id}")
	public ResponseEntity<RespuestaDTO> delete (@PathVariable("id")Long id){
		RespuestaDTO respuesta = new RespuestaDTO();
		try {
			Cliente cliente = clienteService.findById(id);
			clienteService.delete(cliente);
			respuesta.setCodigo("200");
			respuesta.setMensaje("El cliente se eliminó con exito");
			return new ResponseEntity<RespuestaDTO>(respuesta,HttpStatus.OK);
		} catch (Exception e) {
			respuesta.setCodigo("500");
			respuesta.setMensaje(e.getMessage());
			return new ResponseEntity<RespuestaDTO>(respuesta, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	@PutMapping("/update")
	public ResponseEntity<RespuestaDTO> uptade(@RequestBody ClienteDTO clienteDTO){
		RespuestaDTO respuesta =new RespuestaDTO();
		try {
			Cliente cliente = ClienteMapper.clienteDTOtoCliente(clienteDTO);
			clienteService.update(cliente);
			respuesta.setCodigo("200");
			respuesta.setMensaje("El cliente se modificó con exito");
			return new ResponseEntity<RespuestaDTO>(respuesta,HttpStatus.OK);
		} catch (Exception e) {
			respuesta.setCodigo("500");
			respuesta.setMensaje(e.getMessage());
			return new ResponseEntity<RespuestaDTO>(respuesta, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@PostMapping("/login")
	public ResponseEntity<ClienteLoginRespuestaDTO> login(@RequestBody ClienteLoginDTO clienteLoginDTO){
		ClienteLoginRespuestaDTO clienteLoginRespuestaDTO =new ClienteLoginRespuestaDTO();
		try {
			Cliente cliente=clienteService.loginCliente(clienteLoginDTO.getCuenId(),clienteLoginDTO.getClieId() , clienteLoginDTO.getClave());
			ClienteDTO clienteDTO = ClienteMapper.clienteToClienteDTO(cliente);
			clienteLoginRespuestaDTO.setCodigo("200");
			clienteLoginRespuestaDTO.setMensaje("Login exitoso");
			clienteLoginRespuestaDTO.setCliente(clienteDTO);
			return new ResponseEntity<ClienteLoginRespuestaDTO>(clienteLoginRespuestaDTO,HttpStatus.OK);
		} catch (Exception e) {
			clienteLoginRespuestaDTO.setCodigo("500");
			clienteLoginRespuestaDTO.setMensaje(e.getMessage());
			return new ResponseEntity<ClienteLoginRespuestaDTO>(clienteLoginRespuestaDTO, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping("/findClienteDatos/{id}/{tiDoc}/{cuenId}")
	public ResponseEntity<ClienteDTO> findById(@PathVariable("id")Long id,@PathVariable("tiDoc")Long tiDoc, @PathVariable("cuenId")String cuenId) {
		ClienteDTO clienteDTO= new ClienteDTO();
		try {
			Cliente cliente = clienteService.consultarCliente(cuenId, id, tiDoc);
			clienteDTO = ClienteMapper.clienteToClienteDTO(cliente);
			return new ResponseEntity<ClienteDTO>(clienteDTO, HttpStatus.OK);
			
			} catch (Exception e) {
			
			return new ResponseEntity<ClienteDTO>(clienteDTO,HttpStatus.INTERNAL_SERVER_ERROR);
		}
	
	}

}
