package co.edu.usbcali.banco.rest.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.edu.usbcali.banco.domain.Cliente;
import co.edu.usbcali.banco.domain.Cuenta;
import co.edu.usbcali.banco.domain.CuentaRegistrada;
import co.edu.usbcali.banco.rest.dto.CambioClaveDTO;
import co.edu.usbcali.banco.rest.dto.ClienteDTO;
import co.edu.usbcali.banco.rest.dto.CuentaDTO;
import co.edu.usbcali.banco.rest.dto.RegistrarCuentaDTO;
import co.edu.usbcali.banco.rest.dto.RespuestaDTO;
import co.edu.usbcali.banco.rest.mapper.ClienteMapper;
import co.edu.usbcali.banco.rest.mapper.CuentaMapper;
import co.edu.usbcali.banco.services.ClienteService;
import co.edu.usbcali.banco.services.CuentaRegistradaService;
import co.edu.usbcali.banco.services.CuentaService;

@RestController
@CrossOrigin
@RequestMapping("/cuenta")
public class CuentaRestController {
	
	@Autowired
	private CuentaService cuentaService;
	@Autowired
	private CuentaMapper cuentaMapper;
	@Autowired
	private CuentaRegistradaService cuentaRegistradaService;
	
	@GetMapping("/findAll")
	public List<CuentaDTO> findAll(){
		List<CuentaDTO> listaCuentaDTO = new ArrayList<>();
		try {
			List<Cuenta> listaCuenta= cuentaService.findAll();
			listaCuentaDTO =  cuentaMapper.cuentaListToCuentaDTOList(listaCuenta);
		} catch (Exception e) {
		
		}
		return listaCuentaDTO; 
	}
	
	@GetMapping("/findById/{id}")
	public CuentaDTO findById(@PathVariable("id")String id) {
		try {
			Cuenta cuenta = cuentaService.findById(id);
			if(cuenta==null) {
				return null;
			}
			return cuentaMapper.cuentaToCuentaDTO(cuenta);
		} catch (Exception e) {
			// TODO: handle exception
		}
		return null;
	}
	
	@PostMapping("/save")
	public ResponseEntity<RespuestaDTO> save(@RequestBody CuentaDTO cuentaDTO){
		RespuestaDTO respuesta =new RespuestaDTO();
		try {
			Cuenta cuenta = cuentaMapper.cuentaDTOToCuenta(cuentaDTO);
			cuentaService.save(cuenta);
			respuesta.setCodigo("200");
			respuesta.setMensaje("La cuenta se creó con exito");
			return new ResponseEntity<RespuestaDTO>(respuesta,HttpStatus.OK);
		} catch (Exception e) {
			respuesta.setCodigo("500");
			respuesta.setMensaje(e.getMessage());
			return new ResponseEntity<RespuestaDTO>(respuesta, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	@DeleteMapping("/delete/{id}")
	public ResponseEntity<RespuestaDTO> delete (@PathVariable("id")String id){
		RespuestaDTO respuesta = new RespuestaDTO();
		try {
			Cuenta cuenta = cuentaService.findById(id);
			cuentaService.delete(cuenta);
			respuesta.setCodigo("200");
			respuesta.setMensaje("La cuenta se eliminó con exito");
			return new ResponseEntity<RespuestaDTO>(respuesta,HttpStatus.OK);
		} catch (Exception e) {
			respuesta.setCodigo("500");
			respuesta.setMensaje(e.getMessage());
			return new ResponseEntity<RespuestaDTO>(respuesta, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	@PutMapping("/update")
	public ResponseEntity<RespuestaDTO> uptade(@RequestBody CuentaDTO cuentaDTO){
		RespuestaDTO respuesta =new RespuestaDTO();
		try {
			Cuenta cuenta = cuentaMapper.cuentaDTOToCuenta(cuentaDTO);
			cuentaService.update(cuenta);
			respuesta.setCodigo("200");
			respuesta.setMensaje("La cuenta se modificó con exito");
			return new ResponseEntity<RespuestaDTO>(respuesta,HttpStatus.OK);
		} catch (Exception e) {
			respuesta.setCodigo("500");
			respuesta.setMensaje(e.getMessage());
			return new ResponseEntity<RespuestaDTO>(respuesta, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/cuentasCliente/{id}")
	public List<CuentaDTO> findAllCuenta(@PathVariable("id")Long id){
		List<CuentaDTO> listaCuentaDTO = new ArrayList<>();
		try {
			List<Cuenta> listaCuenta= cuentaService.cuentasPorCliente(id);
			listaCuentaDTO =  cuentaMapper.cuentaListToCuentaDTOList(listaCuenta);
		} catch (Exception e) {
		
		}
		return listaCuentaDTO; 
	}
	
	@GetMapping("/cuentasRegistradasCliente/{id}")
	public List<CuentaDTO> findAllCuentasRegistradas(@PathVariable("id")Long id){
		List<CuentaDTO> listaCuentaDTO = new ArrayList<>();
		try {
			List<Cuenta> listaCuenta= cuentaService.cuentasRegistradasPorCliente(id);
			listaCuentaDTO =  cuentaMapper.cuentaListToCuentaDTOList(listaCuenta);
		} catch (Exception e) {
		
		}
		return listaCuentaDTO; 
	}
	
	@PostMapping("/cambioClave")
	public ResponseEntity<RespuestaDTO> cambiarClave(@RequestBody CambioClaveDTO cambioClaveDTO){
		RespuestaDTO respuesta =new RespuestaDTO();
		try {
			cuentaService.cambiarClaveCuenta(cambioClaveDTO.getCuenId(), cambioClaveDTO.getClaveAntigua(), cambioClaveDTO.getClaveNueva());
			respuesta.setCodigo("200");
			respuesta.setMensaje("La clave se cambió con éxito");
			return new ResponseEntity<RespuestaDTO>(respuesta,HttpStatus.OK);
		} catch (Exception e) {
			respuesta.setCodigo("500");
			respuesta.setMensaje(e.getMessage());
			return new ResponseEntity<RespuestaDTO>(respuesta, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@PostMapping("/registrarCuenta")
	public ResponseEntity<RespuestaDTO> registrarCuenta(@RequestBody RegistrarCuentaDTO registrarCuentaDTO){
		RespuestaDTO respuesta =new RespuestaDTO();
		try {
			
			cuentaRegistradaService.registrarCuenta(registrarCuentaDTO.getClieId(), registrarCuentaDTO.getTiDocId(), registrarCuentaDTO.getId(), registrarCuentaDTO.getCuenId());
			respuesta.setCodigo("200");
			respuesta.setMensaje("La Cuenta se registró con éxito");
			return new ResponseEntity<RespuestaDTO>(respuesta,HttpStatus.OK);
		} catch (Exception e) {
			respuesta.setCodigo("500");
			respuesta.setMensaje(e.getMessage());
			return new ResponseEntity<RespuestaDTO>(respuesta, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
}
