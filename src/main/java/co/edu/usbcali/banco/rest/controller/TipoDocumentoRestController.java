package co.edu.usbcali.banco.rest.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.edu.usbcali.banco.domain.TipoDocumento;
import co.edu.usbcali.banco.rest.dto.RespuestaDTO;
import co.edu.usbcali.banco.rest.dto.TipoDocumentoDTO;
import co.edu.usbcali.banco.rest.mapper.TipoDocumentoMapper;
import co.edu.usbcali.banco.services.TipoDocumentoService;

@RestController
@CrossOrigin
@RequestMapping("/tipoDocumento")
public class TipoDocumentoRestController {
	
	@Autowired
	private TipoDocumentoService tipoDocumentoService;
	@Autowired
	private TipoDocumentoMapper tipoDocumentoMapper;
	
	@GetMapping("/findAll")
	public List<TipoDocumentoDTO> findAll(){
		List<TipoDocumentoDTO> tipoDocumentoDTOs = new ArrayList<>();
		try {
			List<TipoDocumento> listaTiposDocumentos= tipoDocumentoService.findAll();
			tipoDocumentoDTOs =  tipoDocumentoMapper.listTipoDocumentotoListTipoDocumentoDTO(listaTiposDocumentos);
		} catch (Exception e) {
		
		}
		return tipoDocumentoDTOs; 
	}
	
	@GetMapping("/findById/{id}")
	public TipoDocumentoDTO findById(@PathVariable("id")Long id) {
		try {
			TipoDocumento tipoDocumento = tipoDocumentoService.findById(id);
			if(tipoDocumento==null) {
				return null;
			}
			return tipoDocumentoMapper.tipoDocumentoToTipoDocumentoDTO(tipoDocumento);
		} catch (Exception e) {
			// TODO: handle exception
		}
		return null;
	}
	@PostMapping("/save")
	public ResponseEntity<RespuestaDTO> save(@RequestBody TipoDocumentoDTO tipoDocumentoDTO){
		RespuestaDTO respuesta =new RespuestaDTO();
		try {
			TipoDocumento tipoDocumento = tipoDocumentoMapper.tipoDocumentoDTOtoTipoDocumento(tipoDocumentoDTO);
			tipoDocumentoService.save(tipoDocumento);
			respuesta.setCodigo("200");
			respuesta.setMensaje("El Tipo de documento se creó con exito");
			return new ResponseEntity<RespuestaDTO>(respuesta,HttpStatus.OK);
		} catch (Exception e) {
			respuesta.setCodigo("500");
			respuesta.setMensaje(e.getMessage());
			return new ResponseEntity<RespuestaDTO>(respuesta, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	@DeleteMapping("/delete/{id}")
	public ResponseEntity<RespuestaDTO> delete (@PathVariable("id")Long id){
		RespuestaDTO respuesta = new RespuestaDTO();
		try {
			TipoDocumento tipoDocumento = tipoDocumentoService.findById(id);
			tipoDocumentoService.delete(tipoDocumento);
			respuesta.setCodigo("200");
			respuesta.setMensaje("El tipo documento se eliminó con exito");
			return new ResponseEntity<RespuestaDTO>(respuesta,HttpStatus.OK);
		} catch (Exception e) {
			respuesta.setCodigo("500");
			respuesta.setMensaje(e.getMessage());
			return new ResponseEntity<RespuestaDTO>(respuesta, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	@PutMapping("/update")
	public ResponseEntity<RespuestaDTO> uptade(@RequestBody TipoDocumentoDTO tipoDocumentoDTO){
		RespuestaDTO respuesta =new RespuestaDTO();
		try {
			TipoDocumento tipoDocumento = tipoDocumentoMapper.tipoDocumentoDTOtoTipoDocumento(tipoDocumentoDTO);
			tipoDocumentoService.update(tipoDocumento);
			respuesta.setCodigo("200");
			respuesta.setMensaje("El tipo documento se modificó con exito");
			return new ResponseEntity<RespuestaDTO>(respuesta,HttpStatus.OK);
		} catch (Exception e) {
			respuesta.setCodigo("500");
			respuesta.setMensaje(e.getMessage());
			return new ResponseEntity<RespuestaDTO>(respuesta, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
