package co.edu.usbcali.banco.rest.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.edu.usbcali.banco.domain.TipoDocumento;
import co.edu.usbcali.banco.domain.TipoUsuario;
import co.edu.usbcali.banco.rest.dto.RespuestaDTO;
import co.edu.usbcali.banco.rest.dto.TipoDocumentoDTO;
import co.edu.usbcali.banco.rest.dto.TipoUsuarioDTO;
import co.edu.usbcali.banco.rest.mapper.TipoDocumentoMapper;
import co.edu.usbcali.banco.rest.mapper.TipoUsuarioMapper;
import co.edu.usbcali.banco.services.TipoDocumentoService;
import co.edu.usbcali.banco.services.TipoUsuarioService;

@RestController
@CrossOrigin
@RequestMapping("/tipoUsuario")
public class TipoUsuarioRestController {
	
	@Autowired
	private TipoUsuarioService tipoUsuarioService;
	@Autowired
	private TipoUsuarioMapper tipoUsuarioMapper;
	
	@GetMapping("/findAll")
	public List<TipoUsuarioDTO> findAll(){
		List<TipoUsuarioDTO> tipoUsuarioDTOs = new ArrayList<>();
		try {
			List<TipoUsuario> listaTiposUsuario= tipoUsuarioService.findAll();
			tipoUsuarioDTOs =  tipoUsuarioMapper.tipoUsuarioListToTipoUsuarioDTOList(listaTiposUsuario);
		} catch (Exception e) {
		
		}
		return tipoUsuarioDTOs; 
	}
	
	@GetMapping("/findById/{id}")
	public TipoUsuarioDTO findById(@PathVariable("id")Long id) {
		try {
			TipoUsuario tipoUsuario = tipoUsuarioService.findById(id);
			if(tipoUsuario==null) {
				return null;
			}
			return tipoUsuarioMapper.tipoUsuarioToTipoUsuarioDTO(tipoUsuario);
		} catch (Exception e) {
			// TODO: handle exception
		}
		return null;
	}
	
	@PostMapping("/save")
	public ResponseEntity<RespuestaDTO> save(@RequestBody TipoUsuarioDTO tipoUsuarioDTO){
		RespuestaDTO respuesta =new RespuestaDTO();
		try {
			TipoUsuario tipoUsuario = tipoUsuarioMapper.tipoUsuarioDTOtoTipoUsuario(tipoUsuarioDTO);
			tipoUsuarioService.save(tipoUsuario);
			respuesta.setCodigo("200");
			respuesta.setMensaje("El Tipo de Usuario se creó con exito");
			return new ResponseEntity<RespuestaDTO>(respuesta,HttpStatus.OK);
		} catch (Exception e) {
			respuesta.setCodigo("500");
			respuesta.setMensaje(e.getMessage());
			return new ResponseEntity<RespuestaDTO>(respuesta, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	@DeleteMapping("/delete/{id}")
	public ResponseEntity<RespuestaDTO> delete (@PathVariable("id")Long id){
		RespuestaDTO respuesta = new RespuestaDTO();
		try {
			TipoUsuario tipoUsuario = tipoUsuarioService.findById(id);
			tipoUsuarioService.delete(tipoUsuario);
			respuesta.setCodigo("200");
			respuesta.setMensaje("El tipo Usuario se eliminó con exito");
			return new ResponseEntity<RespuestaDTO>(respuesta,HttpStatus.OK);
		} catch (Exception e) {
			respuesta.setCodigo("500");
			respuesta.setMensaje(e.getMessage());
			return new ResponseEntity<RespuestaDTO>(respuesta, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	@PutMapping("/update")
	public ResponseEntity<RespuestaDTO> uptade(@RequestBody TipoUsuarioDTO tipoUsuarioDTO){
		RespuestaDTO respuesta =new RespuestaDTO();
		try {
			TipoUsuario tipoUsuario = tipoUsuarioMapper.tipoUsuarioDTOtoTipoUsuario(tipoUsuarioDTO);
			tipoUsuarioService.update(tipoUsuario);
			respuesta.setCodigo("200");
			respuesta.setMensaje("El tipo usuario se modificó con exito");
			return new ResponseEntity<RespuestaDTO>(respuesta,HttpStatus.OK);
		} catch (Exception e) {
			respuesta.setCodigo("500");
			respuesta.setMensaje(e.getMessage());
			return new ResponseEntity<RespuestaDTO>(respuesta, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
}
