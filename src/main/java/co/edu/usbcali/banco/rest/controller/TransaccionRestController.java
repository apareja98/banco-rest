package co.edu.usbcali.banco.rest.controller;



import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.edu.usbcali.banco.domain.Cliente;
import co.edu.usbcali.banco.domain.Cuenta;
import co.edu.usbcali.banco.domain.Transaccion;
import co.edu.usbcali.banco.domain.Usuario;
import co.edu.usbcali.banco.dto.TransaccionDTO;
import co.edu.usbcali.banco.rest.dto.ClienteDTO;
import co.edu.usbcali.banco.rest.dto.RespuestaDTO;
import co.edu.usbcali.banco.rest.dto.TransaccionBancariaDTO;
import co.edu.usbcali.banco.rest.dto.TransaccionRespuestaDTO;
import co.edu.usbcali.banco.rest.dto.TrasladoDTO;
import co.edu.usbcali.banco.rest.dto.UsuarioDTO;
import co.edu.usbcali.banco.rest.mapper.TransaccionMapper;
import co.edu.usbcali.banco.services.TransaccionBancariaService;
import co.edu.usbcali.banco.services.TransaccionService;

@CrossOrigin
@RestController
@RequestMapping("/transaccionBancaria")
public class TransaccionRestController {
	
	@Autowired
	private TransaccionBancariaService transaccionBancariaService;
	@Autowired
	private TransaccionService transaccionService;
	
	@Autowired
	private TransaccionMapper transaccionMapper;
	
	@PostMapping("/consignar")
	public ResponseEntity<TransaccionRespuestaDTO> consignar(@RequestBody TransaccionBancariaDTO transaccion){
		TransaccionRespuestaDTO respuesta =new TransaccionRespuestaDTO();
		try {
			Cuenta cuenta = transaccionBancariaService.consignar(transaccion.getCuenId(), transaccion.getUsuUsuario(), transaccion.getValor());
			respuesta.setCodigo("200");
			respuesta.setMensaje("ConsignaciónExitosa");
			respuesta.setSaldo(transaccion.getValor());
			return new ResponseEntity<TransaccionRespuestaDTO>(respuesta,HttpStatus.OK);
		} catch (Exception e) {
			respuesta.setCodigo("500");
			respuesta.setMensaje(e.getMessage());
			respuesta.setSaldo(null);
			return new ResponseEntity<TransaccionRespuestaDTO>(respuesta, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	@PostMapping("/retirar")
	public ResponseEntity<TransaccionRespuestaDTO> retirar(@RequestBody TransaccionBancariaDTO transaccion){
		TransaccionRespuestaDTO respuesta =new TransaccionRespuestaDTO();
		try {
			Cuenta cuenta = transaccionBancariaService.retirar(transaccion.getCuenId(), transaccion.getUsuUsuario(), transaccion.getValor());
			respuesta.setCodigo("200");
			respuesta.setMensaje("Retiro Realizado con éxito");
			respuesta.setSaldo(cuenta.getSaldo());
			return new ResponseEntity<TransaccionRespuestaDTO>(respuesta,HttpStatus.OK);
		} catch (Exception e) {
			respuesta.setCodigo("500");
			respuesta.setMensaje(e.getMessage());
			respuesta.setSaldo(null);
			return new ResponseEntity<TransaccionRespuestaDTO>(respuesta, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@PostMapping("/retirarATM")
	public ResponseEntity<TransaccionRespuestaDTO> retirarATM(@RequestBody TransaccionBancariaDTO transaccion){
		TransaccionRespuestaDTO respuesta =new TransaccionRespuestaDTO();
		try {
			Cuenta cuenta = transaccionBancariaService.retirarATM(transaccion.getCuenId(),transaccion.getClave(), transaccion.getUsuUsuario(), transaccion.getValor());
			respuesta.setCodigo("200");
			respuesta.setMensaje("Retiro Realizado con éxito");
			respuesta.setSaldo(cuenta.getSaldo());
			return new ResponseEntity<TransaccionRespuestaDTO>(respuesta,HttpStatus.OK);
		} catch (Exception e) {
			respuesta.setCodigo("500");
			respuesta.setMensaje(e.getMessage());
			respuesta.setSaldo(null);
			return new ResponseEntity<TransaccionRespuestaDTO>(respuesta, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@PostMapping("/traslado")
	public ResponseEntity<RespuestaDTO> traslado(@RequestBody TrasladoDTO trasladoDTO){
		RespuestaDTO respuesta =new RespuestaDTO();
		try {
			transaccionBancariaService.traslado(trasladoDTO.getCuenIdOrigen(), trasladoDTO.getCuenIdDestino(), trasladoDTO.getUsuUsuario(), trasladoDTO.getValor());
			respuesta.setCodigo("200");
			respuesta.setMensaje("Traslado Realizado con éxito");
			return new ResponseEntity<RespuestaDTO>(respuesta,HttpStatus.OK);
		} catch (Exception e) {
			respuesta.setCodigo("500");
			respuesta.setMensaje(e.getMessage());
		
			return new ResponseEntity<RespuestaDTO>(respuesta, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	
	@GetMapping("/consultarSaldoATM/{id}/{clave}")
	public ResponseEntity<TransaccionRespuestaDTO> findById(@PathVariable("id")String id, @PathVariable("clave")String clave) {
		TransaccionRespuestaDTO respuesta = new TransaccionRespuestaDTO();
		try {
			Cuenta cuenta = transaccionBancariaService.consultaSaldo(id, clave);
			respuesta.setCodigo("200");
			respuesta.setSaldo(cuenta.getSaldo());
			respuesta.setMensaje("Saldo consultado");
			
			return new ResponseEntity<TransaccionRespuestaDTO>(respuesta,HttpStatus.OK);
		} catch (Exception e) {
			respuesta.setCodigo("500");
			respuesta.setMensaje(e.getMessage());
			respuesta.setSaldo(null);
			return new ResponseEntity<TransaccionRespuestaDTO>(respuesta, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	
	}
	
	
	
	@GetMapping("/ultimas/{id}")
	public List<TransaccionDTO> findAll(@PathVariable("id")Long id){
		List<TransaccionDTO> listaTransaccionesDTO = new ArrayList<>();
		try {
			List<Transaccion> listaTransaccion= transaccionService.transaccionesPorCliente(id);
			listaTransaccionesDTO =  transaccionMapper.transaccionListToDTO(listaTransaccion);
		} catch (Exception e) {
		
		}
		return listaTransaccionesDTO; 
	}
	
	
	@GetMapping("/transaccionesPorCuenta/{id}")
	public List<TransaccionDTO> transaccionesPorCuenta(@PathVariable("id")String id){
		List<TransaccionDTO> listaTransaccionesDTO = new ArrayList<>();
		try {
			List<Transaccion> listaTransaccion= transaccionService.transaccionesPorCuenta(id);
			listaTransaccionesDTO =  transaccionMapper.transaccionListToDTO(listaTransaccion);
		} catch (Exception e) {
		
		}
		return listaTransaccionesDTO; 
	}

	
	

}
