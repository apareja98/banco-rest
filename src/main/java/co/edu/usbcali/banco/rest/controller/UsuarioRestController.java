package co.edu.usbcali.banco.rest.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import co.edu.usbcali.banco.domain.Usuario;
import co.edu.usbcali.banco.rest.dto.RespuestaDTO;
import co.edu.usbcali.banco.rest.dto.UsuarioDTO;
import co.edu.usbcali.banco.rest.mapper.UsuarioMapper;
import co.edu.usbcali.banco.services.UsuarioService;

@RestController
@RequestMapping("/usuario")
@CrossOrigin
public class UsuarioRestController {
	
	@Autowired
	private UsuarioService usuarioService;
	@Autowired
	private UsuarioMapper usuarioMapper;
	
	@GetMapping("/findAll")
	public List<UsuarioDTO> findAll(){
		List<UsuarioDTO> listaUsuarioDTO = new ArrayList<>();
		try {
			List<Usuario> usuarios= usuarioService.findAll();
			listaUsuarioDTO =  usuarioMapper.listaUsuarioToUsuarioDTO(usuarios);
		} catch (Exception e) {
		
		}
		return listaUsuarioDTO; 
	}
	
	@GetMapping("/findById/{id}")
	public UsuarioDTO findById(@PathVariable("id")String id) {
		try {
			Usuario usuario = usuarioService.findById(id);
			if(usuario==null) {
				return null;
			}
			return usuarioMapper.usuarioToUsuarioDTO(usuario);
		} catch (Exception e) {
			// TODO: handle exception
		}
		return null;
	}
	@PostMapping("/save")
	public ResponseEntity<RespuestaDTO> save(@RequestBody UsuarioDTO usuarioDTO){
		RespuestaDTO respuesta =new RespuestaDTO();
		try {
			Usuario usuario = usuarioMapper.usuarioDTOtoUsuario(usuarioDTO);
			usuarioService.save(usuario);
			respuesta.setCodigo("200");
			respuesta.setMensaje("El Usuario se creó con exito");
			return new ResponseEntity<RespuestaDTO>(respuesta,HttpStatus.OK);
		} catch (Exception e) {
			respuesta.setCodigo("500");
			respuesta.setMensaje(e.getMessage());
			return new ResponseEntity<RespuestaDTO>(respuesta, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	@DeleteMapping("/delete/{id}")
	public ResponseEntity<RespuestaDTO> delete (@PathVariable("id")String id){
		RespuestaDTO respuesta = new RespuestaDTO();
		try {
			Usuario usuario = usuarioService.findById(id);
			usuarioService.delete(usuario);
			respuesta.setCodigo("200");
			respuesta.setMensaje("El Usuario se eliminó con exito");
			return new ResponseEntity<RespuestaDTO>(respuesta,HttpStatus.OK);
		} catch (Exception e) {
			respuesta.setCodigo("500");
			respuesta.setMensaje(e.getMessage());
			return new ResponseEntity<RespuestaDTO>(respuesta, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	@PutMapping("/update")
	public ResponseEntity<RespuestaDTO> uptade(@RequestBody UsuarioDTO usuarioDTO){
		RespuestaDTO respuesta =new RespuestaDTO();
		try {
			Usuario usuario = usuarioMapper.usuarioDTOtoUsuario(usuarioDTO);
			usuarioService.update(usuario);
			respuesta.setCodigo("200");
			respuesta.setMensaje("El usuario se modificó con exito");
			return new ResponseEntity<RespuestaDTO>(respuesta,HttpStatus.OK);
		} catch (Exception e) {
			respuesta.setCodigo("500");
			respuesta.setMensaje(e.getMessage());
			return new ResponseEntity<RespuestaDTO>(respuesta, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
