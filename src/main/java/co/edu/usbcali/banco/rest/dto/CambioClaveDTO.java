package co.edu.usbcali.banco.rest.dto;

public class CambioClaveDTO {
		private String cuenId;
		private String claveAntigua; 
		private String claveNueva;
		
		
		
		public CambioClaveDTO() {
			super();
		}
		public String getCuenId() {
			return cuenId;
		}
		public void setCuenId(String cuenId) {
			this.cuenId = cuenId;
		}
		public String getClaveAntigua() {
			return claveAntigua;
		}
		public void setClaveAntigua(String claveAntigua) {
			this.claveAntigua = claveAntigua;
		}
		public String getClaveNueva() {
			return claveNueva;
		}
		public void setClaveNueva(String claveNueva) {
			this.claveNueva = claveNueva;
		}
		

}
