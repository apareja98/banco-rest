package co.edu.usbcali.banco.rest.dto;

public class ClienteLoginDTO {
	private String cuenId; 
	private Long clieId;
	private String clave;
	public String getCuenId() {
		return cuenId;
	}
	public void setCuenId(String cuenId) {
		this.cuenId = cuenId;
	}
	public Long getClieId() {
		return clieId;
	}
	public void setClieId(Long clieId) {
		this.clieId = clieId;
	}
	public String getClave() {
		return clave;
	}
	public void setClave(String clave) {
		this.clave = clave;
	}
	public ClienteLoginDTO() {
		super();
	}
	
}
