package co.edu.usbcali.banco.rest.dto;

public class ClienteLoginRespuestaDTO {
	private String codigo;
	private String mensaje;
	private ClienteDTO cliente;
	public ClienteLoginRespuestaDTO() {
		
	}
	
	public String getCodigo() {
		return codigo;
	}
	
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	
	public String getMensaje() {
		return mensaje;
	}
	
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	
	public ClienteDTO getCliente() {
		return cliente;
	}
	
	public void setCliente(ClienteDTO cliente) {
		this.cliente = cliente;
	}
	
	

}
