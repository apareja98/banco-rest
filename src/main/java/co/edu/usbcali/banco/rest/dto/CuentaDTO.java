package co.edu.usbcali.banco.rest.dto;

import java.math.BigDecimal;

public class CuentaDTO {
	private String cuenId;
	private String activa;
	private String clave;
	private BigDecimal saldo;
	private long clieId;
	private String clienteNombre;
	
	
	
	public CuentaDTO() {
		super();
	}
	public String getCuenId() {
		return cuenId;
	}
	public void setCuenId(String cuenId) {
		this.cuenId = cuenId;
	}
	public String getActiva() {
		return activa;
	}
	public void setActiva(String activa) {
		this.activa = activa;
	}
	public String getClave() {
		return clave;
	}
	public void setClave(String clave) {
		this.clave = clave;
	}
	public BigDecimal getSaldo() {
		return saldo;
	}
	public void setSaldo(BigDecimal saldo) {
		this.saldo = saldo;
	}
	public long getClieId() {
		return clieId;
	}
	public void setClieId(long clieId) {
		this.clieId = clieId;
	}
	public String getClienteNombre() {
		return clienteNombre;
	}
	public void setClienteNombre(String clienteNombre) {
		this.clienteNombre = clienteNombre;
	}
	
}
