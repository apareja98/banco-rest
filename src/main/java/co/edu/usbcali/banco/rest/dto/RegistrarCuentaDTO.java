package co.edu.usbcali.banco.rest.dto;

public class RegistrarCuentaDTO {
	private Long clieId;
	private Long tiDocId;
	private Long id;
	private String cuenId;
	
	
	
	public RegistrarCuentaDTO() {
		super();
	}
	public Long getClieId() {
		return clieId;
	}
	public void setClieId(Long clieId) {
		this.clieId = clieId;
	}
	public Long getTiDocId() {
		return tiDocId;
	}
	public void setTiDocId(Long tiDocId) {
		this.tiDocId = tiDocId;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getCuenId() {
		return cuenId;
	}
	public void setCuenId(String cuenId) {
		this.cuenId = cuenId;
	}
	
	
}
