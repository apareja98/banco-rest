package co.edu.usbcali.banco.rest.dto;


public class TipoDocumentoDTO {
	private Long tdocId;
	private String activo;
	private String nombre;
	public Long getTdocId() {
		return tdocId;
	}
	
	public TipoDocumentoDTO() {
		super();
	}

	public void setTdocId(Long tdocId) {
		this.tdocId = tdocId;
	}
	public String getActivo() {
		return activo;
	}
	public void setActivo(String activo) {
		this.activo = activo;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public TipoDocumentoDTO(Long tdocId, String activo, String nombre) {
		super();
		this.tdocId = tdocId;
		this.activo = activo;
		this.nombre = nombre;
	}
	
	
	
}
