package co.edu.usbcali.banco.rest.dto;

import java.math.BigDecimal;

public class TransaccionBancariaDTO {
	
	private String cuenId;
	private String usuUsuario;
	private BigDecimal valor;
	private String clave;
	
	public String getCuenId() {
		return cuenId;
	}
	public void setCuenId(String cuenId) {
		this.cuenId = cuenId;
	}
	public String getUsuUsuario() {
		return usuUsuario;
	}
	public void setUsuUsuario(String usuUsuario) {
		this.usuUsuario = usuUsuario;
	}
	public BigDecimal getValor() {
		return valor;
	}
	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}
	public TransaccionBancariaDTO(String cuenId, String usuUsuario, BigDecimal valor) {
		super();
		this.cuenId = cuenId;
		this.usuUsuario = usuUsuario;
		this.valor = valor;
	}
	public TransaccionBancariaDTO() {
		
	}
	public String getClave() {
		return clave;
	}
	public void setClave(String clave) {
		this.clave = clave;
	}
	

}
