package co.edu.usbcali.banco.rest.dto;

import java.math.BigDecimal;

public class TransaccionRespuestaDTO {
	private String codigo;
	private String mensaje;
	private BigDecimal saldo;
	public TransaccionRespuestaDTO(String codigo, String mensaje, BigDecimal saldo) {
		super();
		this.codigo = codigo;
		this.mensaje = mensaje;
		this.saldo = saldo;
	}
	public TransaccionRespuestaDTO() {
		super();
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	public BigDecimal getSaldo() {
		return saldo;
	}
	public void setSaldo(BigDecimal saldo) {
		this.saldo = saldo;
	}
	
}
