package co.edu.usbcali.banco.rest.dto;

import java.math.BigDecimal;

public class TrasladoDTO {
	private String cuenIdOrigen;
	private String cuenIdDestino;
	private String usuUsuario;
	private BigDecimal valor;
	public String getCuenIdOrigen() {
		return cuenIdOrigen;
	}
	public void setCuenIdOrigen(String cuenIdOrigen) {
		this.cuenIdOrigen = cuenIdOrigen;
	}
	public String getCuenIdDestino() {
		return cuenIdDestino;
	}
	public void setCuenIdDestino(String cuenIdDestino) {
		this.cuenIdDestino = cuenIdDestino;
	}
	public String getUsuUsuario() {
		return usuUsuario;
	}
	public void setUsuUsuario(String usuUsuario) {
		this.usuUsuario = usuUsuario;
	}
	public BigDecimal getValor() {
		return valor;
	}
	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}
	public TrasladoDTO() {
		super();
	}
	
	

}
