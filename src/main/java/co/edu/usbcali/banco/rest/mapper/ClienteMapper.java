package co.edu.usbcali.banco.rest.mapper;

import java.util.List;

import co.edu.usbcali.banco.domain.Cliente;
import co.edu.usbcali.banco.rest.dto.ClienteDTO;

public interface ClienteMapper {
	public Cliente clienteDTOtoCliente(ClienteDTO clienteDTO)throws Exception;
	public ClienteDTO clienteToClienteDTO (Cliente cliente)throws Exception;
	public List<ClienteDTO> listaClienteToClienteDTO (List<Cliente> listaCliente)throws Exception;
	
	
}
