package co.edu.usbcali.banco.rest.mapper;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.core.util.ArrayUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.edu.usbcali.banco.domain.Cliente;
import co.edu.usbcali.banco.domain.TipoDocumento;
import co.edu.usbcali.banco.rest.dto.ClienteDTO;
import co.edu.usbcali.banco.services.ClienteService;
import co.edu.usbcali.banco.services.TipoDocumentoService;

@Component
@Scope("singleton")
public class ClienteMapperImpl implements ClienteMapper {
	
	@Autowired
	private ClienteService clienteService;

	@Autowired
	private TipoDocumentoService tipoDocumentoService;

	@Override
	public Cliente clienteDTOtoCliente(ClienteDTO clienteDTO) throws Exception {
		Cliente cliente = new Cliente();
		cliente.setActivo(clienteDTO.getActivo());
		cliente.setClieId(clienteDTO.getClieId());
		cliente.setDireccion(clienteDTO.getDireccion());
		cliente.setEmail(clienteDTO.getEmail());
		cliente.setNombre(clienteDTO.getNombre());
		cliente.setTelefono(clienteDTO.getTelefono());
		TipoDocumento tiDoc = tipoDocumentoService.findById(clienteDTO.getTdocIdTipoDocumento());
		cliente.setTipoDocumento(tiDoc);
		return cliente;
	}

	@Override
	public ClienteDTO clienteToClienteDTO(Cliente cliente) throws Exception {
		ClienteDTO clienteDTO = new ClienteDTO();
		clienteDTO.setActivo(cliente.getActivo());
		clienteDTO.setClieId(cliente.getClieId());
		clienteDTO.setDireccion(cliente.getDireccion());
		clienteDTO.setEmail(cliente.getEmail());
		clienteDTO.setNombre(cliente.getNombre());
		TipoDocumento tipoDocumento = tipoDocumentoService.findById(cliente.getTipoDocumento().getTdocId());
		clienteDTO.setTelefono(cliente.getTelefono());
		clienteDTO.setActivoTipoDocumento(tipoDocumento.getActivo());
		clienteDTO.setNombreTipoDocumento(tipoDocumento.getNombre());
		clienteDTO.setTdocIdTipoDocumento(tipoDocumento.getTdocId());

		return clienteDTO;
	}

	@Override
	public List<ClienteDTO> listaClienteToClienteDTO(List<Cliente> listaCliente) throws Exception {
		List<ClienteDTO> listaClienteDTO = new ArrayList<>();
		for (Cliente cliente : listaCliente) {
			listaClienteDTO.add(clienteToClienteDTO(cliente));
		}
		return listaClienteDTO;
	}
	
	

}
