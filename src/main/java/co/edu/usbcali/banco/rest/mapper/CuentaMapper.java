package co.edu.usbcali.banco.rest.mapper;

import java.util.List;

import co.edu.usbcali.banco.domain.Cuenta;
import co.edu.usbcali.banco.rest.dto.CuentaDTO;

public interface CuentaMapper {
	
	public CuentaDTO cuentaToCuentaDTO (Cuenta cuenta);
	
	public Cuenta cuentaDTOToCuenta(CuentaDTO cuentaDTO) throws Exception;
	
	public List<CuentaDTO> cuentaListToCuentaDTOList(List<Cuenta> cuentas);

}
