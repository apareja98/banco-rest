package co.edu.usbcali.banco.rest.mapper;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.edu.usbcali.banco.domain.Cliente;
import co.edu.usbcali.banco.domain.Cuenta;
import co.edu.usbcali.banco.rest.dto.CuentaDTO;
import co.edu.usbcali.banco.services.ClienteService;
import co.edu.usbcali.banco.services.CuentaService;

@Component
@Scope("singleton")
public class CuentaMapperImpl implements CuentaMapper{
	@Autowired
	CuentaService cuentaService;
	@Autowired
	ClienteService clienteService;

	@Override
	public CuentaDTO cuentaToCuentaDTO(Cuenta cuenta) {
		CuentaDTO cuentaDTO = new CuentaDTO();
		cuentaDTO.setActiva(cuenta.getActiva());
		cuentaDTO.setClave(cuenta.getClave());
		cuentaDTO.setClieId(cuenta.getCliente().getClieId());
		cuentaDTO.setClienteNombre(cuenta.getCliente().getNombre());
		cuentaDTO.setCuenId(cuenta.getCuenId());
		cuentaDTO.setSaldo(cuenta.getSaldo());
		return cuentaDTO;
	}

	@Override
	public Cuenta cuentaDTOToCuenta(CuentaDTO cuentaDTO) throws Exception {
		Cuenta cuenta = new Cuenta();
		cuenta.setActiva(cuentaDTO.getActiva());
		cuenta.setClave(cuentaDTO.getClave());
		Cliente cliente = clienteService.findById(cuentaDTO.getClieId());
		cuenta.setCliente(cliente);
		cuenta.setCuenId(cuentaDTO.getCuenId());
		cuenta.setSaldo(cuentaDTO.getSaldo());
		return cuenta;
	}

	@Override
	public List<CuentaDTO> cuentaListToCuentaDTOList(List<Cuenta> cuentas) {
		List<CuentaDTO> listaCuentaDTO = new ArrayList<>();
		for (Cuenta cuenta : cuentas) {
			CuentaDTO cuentaDTO = new CuentaDTO();
			cuentaDTO.setActiva(cuenta.getActiva());
			cuentaDTO.setClave(cuenta.getClave());
			cuentaDTO.setClieId(cuenta.getCliente().getClieId());
			cuentaDTO.setClienteNombre(cuenta.getCliente().getNombre());
			cuentaDTO.setCuenId(cuenta.getCuenId());
			cuentaDTO.setSaldo(cuenta.getSaldo());
			listaCuentaDTO.add(cuentaDTO);
		}
		return listaCuentaDTO;
	}
	
	

}
