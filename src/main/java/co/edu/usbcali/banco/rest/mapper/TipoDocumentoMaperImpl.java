package co.edu.usbcali.banco.rest.mapper;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.edu.usbcali.banco.domain.TipoDocumento;
import co.edu.usbcali.banco.rest.dto.TipoDocumentoDTO;
import co.edu.usbcali.banco.services.TipoDocumentoService;

@Component
@Scope("singleton")
public class TipoDocumentoMaperImpl implements TipoDocumentoMapper {
	
	@Autowired
	private TipoDocumentoService tipoDocumentoService;

	@Override
	public TipoDocumentoDTO tipoDocumentoToTipoDocumentoDTO(TipoDocumento tipoDocumento) throws Exception {
		TipoDocumentoDTO tipoDocumentoDTO = new TipoDocumentoDTO();
		tipoDocumentoDTO.setActivo(tipoDocumento.getActivo());
		tipoDocumentoDTO.setNombre(tipoDocumento.getNombre());
		tipoDocumentoDTO.setTdocId(tipoDocumento.getTdocId());
		return tipoDocumentoDTO;
	}

	@Override
	public TipoDocumento tipoDocumentoDTOtoTipoDocumento(TipoDocumentoDTO tipoDocumentoDTO) throws Exception {
		TipoDocumento tipoDocumento = new TipoDocumento();
		tipoDocumento.setActivo(tipoDocumentoDTO.getActivo());
		tipoDocumento.setNombre(tipoDocumentoDTO.getNombre());
		tipoDocumento.setTdocId(tipoDocumentoDTO.getTdocId());
		return tipoDocumento;
	}

	@Override
	public List<TipoDocumentoDTO> listTipoDocumentotoListTipoDocumentoDTO(List<TipoDocumento> lista) throws Exception {
		List<TipoDocumentoDTO>tipoDocumentoDTOlista= new ArrayList<>();
		for (TipoDocumento tipoDocumento : lista) {
			tipoDocumentoDTOlista.add(tipoDocumentoToTipoDocumentoDTO(tipoDocumento));
		}
		return tipoDocumentoDTOlista;
	}

	@Override
	public List<TipoDocumento> listTipoDocumentoDTOtoListTipoDocumento(List<TipoDocumentoDTO> lista) throws Exception {
		List<TipoDocumento> tipoDocumentos = new ArrayList<>();
		
		for (TipoDocumentoDTO tipoDocumentoDTO : lista) {
			tipoDocumentos.add(tipoDocumentoDTOtoTipoDocumento(tipoDocumentoDTO));
		}
		return tipoDocumentos;
	}

}
