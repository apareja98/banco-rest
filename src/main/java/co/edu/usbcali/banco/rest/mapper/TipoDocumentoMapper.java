package co.edu.usbcali.banco.rest.mapper;

import java.util.List;

import co.edu.usbcali.banco.domain.TipoDocumento;
import co.edu.usbcali.banco.rest.dto.TipoDocumentoDTO;

public interface TipoDocumentoMapper {
	
	public TipoDocumentoDTO tipoDocumentoToTipoDocumentoDTO(TipoDocumento tipoDocumento)throws Exception;
	
	public TipoDocumento tipoDocumentoDTOtoTipoDocumento(TipoDocumentoDTO tipoDocumentoDTO)throws Exception;

	public List<TipoDocumentoDTO> listTipoDocumentotoListTipoDocumentoDTO (List<TipoDocumento> lista)throws Exception;
	
	public List<TipoDocumento> listTipoDocumentoDTOtoListTipoDocumento (List<TipoDocumentoDTO> lista)throws Exception;
	
}
