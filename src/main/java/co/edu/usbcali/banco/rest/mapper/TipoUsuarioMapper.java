package co.edu.usbcali.banco.rest.mapper;

import java.util.List;

import co.edu.usbcali.banco.domain.TipoUsuario;
import co.edu.usbcali.banco.rest.dto.TipoUsuarioDTO;

public interface TipoUsuarioMapper {
	
	public TipoUsuarioDTO tipoUsuarioToTipoUsuarioDTO(TipoUsuario tipoUsuario);
	
	public TipoUsuario tipoUsuarioDTOtoTipoUsuario(TipoUsuarioDTO tipoUsuarioDTO);
	
	public List<TipoUsuarioDTO> tipoUsuarioListToTipoUsuarioDTOList(List<TipoUsuario> listTipoUsuario);
	
	public List<TipoUsuario> tipoUsuarioDTOListToTipoUsuarioList(List<TipoUsuarioDTO> listTipoUsuarioDTO);

}
