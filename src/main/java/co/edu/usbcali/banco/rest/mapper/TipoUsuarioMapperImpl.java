package co.edu.usbcali.banco.rest.mapper;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.edu.usbcali.banco.domain.TipoUsuario;
import co.edu.usbcali.banco.rest.dto.TipoUsuarioDTO;
import co.edu.usbcali.banco.services.TipoUsuarioService;

@Component
@Scope("singleton")
public class TipoUsuarioMapperImpl implements TipoUsuarioMapper {
	
	@Autowired
	TipoUsuarioService tipoUsuarioService;
	
	@Override
	public TipoUsuarioDTO tipoUsuarioToTipoUsuarioDTO(TipoUsuario tipoUsuario) {
		TipoUsuarioDTO tipoUsuarioDTO =  new TipoUsuarioDTO();
		tipoUsuarioDTO.setActivo(tipoUsuario.getActivo());
		tipoUsuarioDTO.setNombre(tipoUsuario.getNombre());
		tipoUsuarioDTO.setTiusId(tipoUsuario.getTiusId());
		return tipoUsuarioDTO;
	}

	@Override
	public TipoUsuario tipoUsuarioDTOtoTipoUsuario(TipoUsuarioDTO tipoUsuarioDTO) {
		TipoUsuario tipoUsuario = new TipoUsuario();
		tipoUsuario.setActivo(tipoUsuarioDTO.getActivo());
		tipoUsuario.setNombre(tipoUsuarioDTO.getNombre());
		tipoUsuario.setTiusId(tipoUsuarioDTO.getTiusId());
		return tipoUsuario;
	}

	@Override
	public List<TipoUsuarioDTO> tipoUsuarioListToTipoUsuarioDTOList(List<TipoUsuario> listTipoUsuario) {
		List<TipoUsuarioDTO> listaRetorno = new ArrayList<>();
		for (TipoUsuario tipoUsuario : listTipoUsuario) {
			TipoUsuarioDTO tipoUsuarioDTO = new TipoUsuarioDTO();
			tipoUsuarioDTO.setActivo(tipoUsuario.getActivo());
			tipoUsuarioDTO.setNombre(tipoUsuario.getNombre());
			tipoUsuarioDTO.setTiusId(tipoUsuario.getTiusId());
			listaRetorno.add(tipoUsuarioDTO);
		}
		
		return listaRetorno;
	}

	@Override
	public List<TipoUsuario> tipoUsuarioDTOListToTipoUsuarioList(List<TipoUsuarioDTO> listTipoUsuarioDTO) {
		List< TipoUsuario> tiposUsuarios = new ArrayList<>();
		for (TipoUsuarioDTO tipoUsuarioDTO : listTipoUsuarioDTO) {
			TipoUsuario tipoUsuario =  new TipoUsuario();
			tipoUsuario.setActivo(tipoUsuarioDTO.getActivo());
			tipoUsuario.setNombre(tipoUsuarioDTO.getNombre());
			tipoUsuario.setTiusId(tipoUsuarioDTO.getTiusId());
			tiposUsuarios.add(tipoUsuario);
		}
		return tiposUsuarios;
	}

}
