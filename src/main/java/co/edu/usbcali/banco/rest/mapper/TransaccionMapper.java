package co.edu.usbcali.banco.rest.mapper;

import java.util.List;

import co.edu.usbcali.banco.domain.Cuenta;
import co.edu.usbcali.banco.domain.Transaccion;
import co.edu.usbcali.banco.dto.TransaccionDTO;
import co.edu.usbcali.banco.rest.dto.CuentaDTO;

public interface TransaccionMapper {
	
	public TransaccionDTO transaccionToTransaccionDTO (Transaccion transaccion);
	
	
	public List<TransaccionDTO> transaccionListToDTO(List<Transaccion> transaccions);

}
