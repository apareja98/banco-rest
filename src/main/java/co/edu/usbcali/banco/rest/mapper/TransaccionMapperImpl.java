package co.edu.usbcali.banco.rest.mapper;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.edu.usbcali.banco.domain.Transaccion;
import co.edu.usbcali.banco.dto.TransaccionDTO;
import co.edu.usbcali.banco.services.TransaccionService;


@Component
@Scope("singleton")
public class TransaccionMapperImpl implements TransaccionMapper {

	@Autowired
	TransaccionService transaccionService;
	
	@Override
	public TransaccionDTO transaccionToTransaccionDTO(Transaccion transaccion) {
		TransaccionDTO transaccionDTO = new TransaccionDTO();
		transaccionDTO.setCuenId(transaccion.getCuenta().getCuenId());
		transaccionDTO.setFecha(transaccion.getFecha());
		transaccionDTO.setNombreTipoTransaccion(transaccion.getTipoTransaccion().getNombre());
		transaccionDTO.setNombreTipoUsuario(transaccion.getTipoTransaccion().getNombre());
		transaccionDTO.setTranId(transaccion.getTranId());
		transaccionDTO.setUsuUsuario(transaccion.getUsuario().getNombre());
		transaccionDTO.setValor(transaccion.getValor());
		return transaccionDTO;
	}

	@Override
	public List<TransaccionDTO> transaccionListToDTO(List<Transaccion> transaccions) {
		List<TransaccionDTO> transacciones = new ArrayList<>();
		for (Transaccion transaccion : transaccions) {
			TransaccionDTO tran = transaccionToTransaccionDTO(transaccion);
			transacciones.add(tran);
		}
		return transacciones;
	}

}
