package co.edu.usbcali.banco.rest.mapper;

import java.util.List;

import co.edu.usbcali.banco.domain.Usuario;
import co.edu.usbcali.banco.rest.dto.UsuarioDTO;

public interface UsuarioMapper {
	public Usuario usuarioDTOtoUsuario(UsuarioDTO usuarioDTO)throws Exception;
	public UsuarioDTO usuarioToUsuarioDTO (Usuario usuario)throws Exception;
	public List<UsuarioDTO> listaUsuarioToUsuarioDTO (List<Usuario> usuarios)throws Exception;
	
	
}
