package co.edu.usbcali.banco.rest.mapper;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import co.edu.usbcali.banco.domain.TipoUsuario;
import co.edu.usbcali.banco.domain.Usuario;
import co.edu.usbcali.banco.rest.dto.UsuarioDTO;
import co.edu.usbcali.banco.services.TipoUsuarioService;

@Component
@Scope("singleton")
public class UsuarioMapperImpl implements UsuarioMapper {

	@Autowired
	private TipoUsuarioService tipoUsuarioService;
	
	@Override
	public Usuario usuarioDTOtoUsuario(UsuarioDTO usuarioDTO) throws Exception {
		Usuario usuario = new Usuario();
		usuario.setActivo(usuarioDTO.getActivo());
		usuario.setClave(usuarioDTO.getClave());
		usuario.setIdentificacion(usuarioDTO.getIdentificacion());
		usuario.setNombre(usuarioDTO.getNombre());
		usuario.setUsuUsuario(usuarioDTO.getUsuUsuario());
		TipoUsuario tipoUsuario = tipoUsuarioService.findById(usuarioDTO.getTipoUsuarioId());
		usuario.setTipoUsuario(tipoUsuario);
		return usuario;
	}

	@Override
	public UsuarioDTO usuarioToUsuarioDTO(Usuario usuario) throws Exception {
		UsuarioDTO usuarioDTO = new UsuarioDTO(); 
		usuarioDTO.setActivo(usuario.getActivo());
		usuarioDTO.setClave(usuario.getClave());
		usuarioDTO.setIdentificacion(usuario.getIdentificacion());
		usuarioDTO.setNombre(usuario.getNombre());
		TipoUsuario tipoUsuario = tipoUsuarioService.findById(usuario.getTipoUsuario().getTiusId());
		usuarioDTO.setTipoUsuarioActivo(tipoUsuario.getActivo());
		usuarioDTO.setTipoUsuarioId(tipoUsuario.getTiusId());
		usuarioDTO.setTipoUsuarioNombre(tipoUsuario.getNombre());
		usuarioDTO.setUsuUsuario(usuario.getUsuUsuario());
		return usuarioDTO;
	}

	@Override
	public List<UsuarioDTO> listaUsuarioToUsuarioDTO(List<Usuario> usuarios) throws Exception {
		List<UsuarioDTO> usuariosDTO = new ArrayList<>();
		for(Usuario usuario : usuarios) {
			UsuarioDTO aux = usuarioToUsuarioDTO(usuario);
			usuariosDTO.add(aux);
		}
		return usuariosDTO;
	}

}
